import fs from 'fs'
import { execSync } from 'child_process'
// eslint-disable-next-line @typescript-eslint/no-var-requires
const MeCab = require('mecab-async')
const { readFile, writeFile } = fs.promises
const mecabDicDir = execSync('mecab-config --dicdir').toString().trim()
const mecabExecDir = execSync('mecab-config --libexecdir').toString().trim()

type orderedItem = {
  id: string
  type: string
  actor: string
  published: string
  to: string[]
  cc: string[]
  object:
    | string
    | {
        id: string
        type: string
        summary: string | null
        inReplyTo: string | null
        published: string
        url: string
        attributedTo: string
        to: string[]
        cc: string[]
        sensitive: boolean
        atomUri: string
        inReplyToAtomUri: string | null
        conversation: string
        content: string
        contentMap: {
          ja?: string
        }
        attachment: []
        tag: string[]
        replies: {
          id: string
          type: string
          first: {
            type: string
            next: string
            partOf: string
            items: []
          }
        }
      }
  signature: {
    type: string
    creator: string
    created: string
    signatureValue: string
  }
}

type tweetJs = {
  tweet: {
    retweeted: boolean
    source: string
    entities: {
      user_mentions: {
        name: string
        screen_name: string
        indices: [string, string]
        id_str: string
        id: string
      }[]
      urls: {
        url: string
        expanded_url: string
        display_url: string
        indices: [string, string]
      }[]
      symbols: []
      media: {
        expanded_url: string
        source_status_id: string
        indices: [string, string]
        url: string
        media_url: string
        id_str: string
        source_user_id: string
        id: string
        media_url_https: string
        source_user_id_str: string
        sizes: {
          thumb: {
            w: string
            h: string
            resize: string
          }
          medium: {
            w: string
            h: string
            resize: string
          }
          small: {
            w: string
            h: string
            resize: string
          }
          large: {
            w: string
            h: string
            resize: string
          }
        }
        type: string
        source_status_id_str: string
        display_url: string
      }[]
      hashtags: string[]
    }
    display_text_range: [string, string]
    favorite_count: string
    id_str: string
    truncated: boolean
    retweet_count: string
    id: string
    possibly_sensitive: boolean
    created_at: string
    favorited: boolean
    full_text: string
    lang: string
    extended_entities: {
      media: {
        expanded_url: string
        source_status_id: string
        indices: [string, string]
        url: string
        media_url: string
        id_str: string
        video_info: {
          aspect_ratio: [string, string]
          duration_millis: string
          variants: {
            bitrate?: string
            content_type: string
            url: string
          }[]
        }
        source_user_id: string
        additional_media_info: {
          monetizable: boolean
        }
        id: string
        media_url_https: string
        source_user_id_str: string
        sizes: {
          thumb: {
            w: string
            h: string
            resize: string
          }
          medium: {
            w: string
            h: string
            resize: string
          }
          small: {
            w: string
            h: string
            resize: string
          }
          large: {
            w: string
            h: string
            resize: string
          }
        }
        type: string
        source_status_id_str: string
        display_url: string
      }[]
    }
  }
}[]

const mecab = new MeCab()
mecab.command = `mecab -d ${mecabDicDir}/mecab-ipadic-neologd/ -u ${__dirname}/udic.dic`
// mecab.command = `mecab -d ${mecabDicDir}/mecab-ipadic-neologd/`

async function pre(): Promise<void> {
  console.log('create user dict...')
  const dic = await readFile('./assets/udic.csv', 'utf-8').then((raw) =>
    raw.trim()
  )
  await writeFile('./assets/udic.csv', dic, 'utf-8')

  execSync(`${mecabExecDir}/mecab-dict-index \
-d ${mecabDicDir}/mecab-ipadic-neologd/ \
-u ${__dirname}/udic.dic \
-f utf-8 \
-t utf-8 \
${__dirname}/udic.csv`)
  console.log('created user dict!')
}

function sanitize(str: string): string {
  const reMap = {
    '&amp;': '&',
    '&gt;': '>',
    '&lt;': '<',
    '&quot;': '"',
    '&apos;': "'",
    '\n': ' ',
    '\\n': ' ',
  }
  return str
    .replace(/<.*?>/g, '')
    .replace(/@\S+/g, '')
    .replace(/#\S+/g, '')
    .replace(/h?ttps?:\/\/\S+/g, '')
    .replace(
      /(&amp;|&gt;|&lt;|&quot;|&apos;|\n|\\n)/g,
      (substr) => reMap[substr as keyof typeof reMap]
    )
    .trim()
}

async function mastodon(): Promise<void> {
  const outbox = await readFile('./assets/outbox.json', 'utf-8')
    .then((raw) => JSON.parse(raw))
    .then((json) =>
      (json.orderedItems as orderedItem[])
        .filter(
          (item) =>
            !item.to.includes(
              'https://social.mikutter.hachune.net/users/sota_n/followers'
            )
        )
        .map((item) =>
          typeof item.object !== 'string' ? item.object.content : ''
        )
        .filter((i) => i != '') // fazy
        // NOTE: swarm 避け
        .filter((i) => !/\(@ .*? in .*?\)/.test(i))
        .map(sanitize)
        .map((str) => mecab.wakachiSync(str).join(' '))
        .map((str) => str.replace(/: (\w+?) :/g, ':$1:'))
    )

  // console.log(outbox)
  writeFile('./assets/words.txt', outbox.join('\n'), 'utf-8')
}

async function twitter(): Promise<void> {
  const tweets = await readFile('./assets/tweet.js', 'utf-8')
    .then((raw) => raw.replace(/window\.YTD\.tweet\.part0 = /, ''))
    .then((str) => JSON.parse(str))
    .then((json: tweetJs) =>
      json
        .filter((obj) => obj.tweet.entities.user_mentions.length === 0)
        // NOTE: swarm 避け。通常テキストだと t\.co に短縮されてしまうので entities を探索に行く
        .filter(
          (obj) =>
            obj.tweet.entities.urls.filter((url) =>
              /swarmapp\.com/.test(url.display_url || '')
            ).length == 0
        )
        .map((obj) => obj.tweet.full_text)
        .map(sanitize)
        .filter((str) => str !== '')
        .map((str) => mecab.wakachiSync(str).join(' '))
    )
  // console.log(tweets)
  writeFile('./assets/tweets.txt', tweets.join('\n'), 'utf-8')
}

pre()
Promise.all([mastodon(), twitter()])
