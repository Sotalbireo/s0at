import fs from 'fs'
import { isLatin } from './Fnc'
import './Fnc'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const markovTwitter = require('node-markovify').markovTwitter

type MarkofTwitterOptions = {
  tweets: string[]
  state_size?: number
  numTweetsToPredict?: number
  popularFirstWord?: boolean
  max_chars?: number
}

export class Markov {
  private generatedTweets: string[] = []
  private markov: typeof markovTwitter
  private option: MarkofTwitterOptions
  private tweets: string[]
  private ngWords: (string | RegExp)[] = ['パクトゥ、パクチーっぽさ', /^:don:/]
  constructor() {
    const mstdn = fs.readFileSync('./assets/words.txt', 'utf-8').split('\n')
    const twttr = fs.readFileSync('./assets/tweets.txt', 'utf-8').split('\n')
    this.tweets = [...mstdn, ...twttr].filter((tweet: string) => tweet !== '')
    this.option = {
      tweets: this.tweets,
      max_chars: 75,
    }
    this.markov = new markovTwitter(this.option)
    this.generate()
  }
  private generate(): void {
    this.markov.generateMarkovTweets(this.option, (tweets: string[]) => {
      fs.appendFile('./markov-logs.txt', tweets.join('\n'), () => {
        /* noop */
      })
      this.generatedTweets = tweets.map((tweet: string) =>
        tweet
          .split(' ')
          .bracketOptimise()
          .map((str, idx, tw) =>
            isLatin(str) &&
            isLatin(tw[idx + 1] || '') &&
            !/^['"`]$/.test(str) &&
            !/^['"`]$/.test(tw[idx + 1] || '') &&
            !/^\.$/.test(tw[idx + 1] || '')
              ? `${str} `
              : str
          )
          .join('')
          .replace(/(:.*?:)/g, ' $1 ')
          .trim()
      )
    })
  }
  pickTweet(force = false): string {
    const isCached = this.generatedTweets.length !== 0
    if (!isCached || force) {
      this.generate()
    }
    const proposedText = this.generatedTweets.shift() || ''
    if (
      this.ngWords.some((ngWord) => {
        if (ngWord instanceof RegExp) {
          return ngWord.test(proposedText)
        } else if (typeof ngWord === 'string') {
          return proposedText.includes(ngWord)
        } else {
          throw new Error(`[Type error] ngWord: <${typeof ngWord}>${ngWord}`)
        }
      })
    ) {
      console.log(`Censored: "${proposedText}".`)
      return this.pickTweet()
    }
    return proposedText
  }
}

// const markov = new Markov()
// Array(20)
//   .fill(0)
//   .forEach(() => console.log(markov.pickTweet()))
