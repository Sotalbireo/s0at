import fetch from 'node-fetch'

export class Butimili {
  private api = 'https://raw.butimi.li/'
  private text = 'エンドポイントからの取得に失敗したらしいっすよ？'
  private suspects: Set<string> = new Set()
  constructor() {
    fetch(this.api)
      .then(async (res) => {
        this.text = await res.text()
      })
      .catch((reason) => {
        console.log(reason)
      })
  }

  btml(suspectId: string): string {
    if (this.suspects.has(suspectId)) {
      this.suspects.delete(suspectId)
    }

    const suspectsArray = [...this.suspects]
    const withinUsers: Set<string> = new Set()
    const withinNum = Math.floor(Math.random() * this.suspects.size)
    while (withinUsers.size < withinNum) {
      withinUsers.add(
        suspectsArray[Math.floor(Math.random() * this.suspects.size)]
      )
    }

    this.suspects.add(suspectId)

    return [...withinUsers, this.text].join(' ').trim()
  }
}
