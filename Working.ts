import { promises as fs } from 'fs'
import { resolve } from 'path'
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'

dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.tz.setDefault(process.env.TIMEZONE)

type timecardJson = {
  since: {
    from: number
    to: number
  }
  stamps: stamp[]
}

type stamp = {
  start: number
  end: number
  rest: stampRest[]
  memo: string
  tags: string[]
}

type stampRest = {
  start: number
  end: number
}

export class Working {
  private records!: timecardJson
  private recordsLength!: number
  private recordsLimitLength = 300
  latestStamp?: stamp

  constructor() {
    this.initialize()
  }

  attendance(ctx: string): [boolean, string] {
    const { memo, tags } = this.parseContext(ctx)
    if (this.latestStamp !== undefined) {
      return [false, '[ERR] 前回の労働が終わっていない']
    }

    this.latestStamp = {
      start: dayjs().valueOf(),
      end: -1,
      rest: [],
      memo,
      tags: [...new Set(tags)],
    }

    return [true, dayjs(this.latestStamp.start).tz().format('H:mm:ss ZZ')]
  }

  leaving(ctx: string): [boolean, string] {
    const { memo, tags } = this.parseContext(ctx)
    if (this.latestStamp === undefined) {
      return [false, '[ERR] 労働開始前に終了処理をしている']
    }

    this.latestStamp.memo += ' ' + memo
    this.latestStamp.memo = this.latestStamp.memo.trim()
    this.latestStamp.tags = [
      ...new Set([...this.latestStamp.tags, ...tags])
    ].sort()
    this.latestStamp.end = dayjs().valueOf()

    const [flg, msg] = this.append()
    if (flg) {
      this.save()
      const worktime = dayjs(this.latestStamp.end - this.latestStamp.start)
        .utc()
        .format('H時間m分s秒')
      this.latestStamp = undefined
      return [true, worktime]
    } else {
      return [false, msg]
    }
  }

  private append(): [boolean, string] {
    if (!this.latestStamp) {
      return [false, '[ERR] レコードがないのに追加処理をしている']
    }
    if (this.latestStamp.end === -1) {
      return [false, '[ERR] レコード終了前に追加処理をしている']
    }
    if (this.latestStamp?.start >= this.latestStamp?.end) {
      return [false, '[ERR] 開始時刻より終了時刻の方が若い']
    }
    this.records.stamps.push(this.latestStamp)
    this.recordsLength = this.records.stamps.length

    if (this.recordsLength === 1) {
      this.records.since.from = this.latestStamp.start
    }
    this.records.since.to = this.latestStamp.end

    return [true, '']
  }

  private async save() {
    if (this.recordsLength > this.recordsLimitLength) {
      await this.write(
        JSON.stringify(this.records),
        this.records.since.to.toString(16)
      )
      this.write('{}')
      this.records.since = { to: -1, from: -1 }
      this.records.stamps = []
    } else {
      this.write(JSON.stringify(this.records))
    }
  }

  private async initialize() {
    this.records = JSON.parse(
      await fs
        .readFile(resolve(__dirname, 'logs/timecard.json'), 'utf-8')
        .catch(async () => {
          await fs.mkdir(resolve(__dirname, 'logs'), { recursive: true })
          await this.write(
            JSON.stringify({ since: { from: -1, to: -1 }, stamps: [] })
          )
          return JSON.stringify({ since: { from: -1, to: -1 }, stamps: [] })
        })
    ) as timecardJson

    this.recordsLength = this.records.stamps?.length ?? 0
  }

  private parseContext(ctx: string) {
    const re = /\$\S*/g
    const tags = ctx.match(re)?.map((tag) => tag.replace('$', '')) ?? []
    return {
      tags,
      memo: ctx.replace(re, '').trim(),
    }
  }

  private write(data: string, post = '') {
    return fs.writeFile(
      resolve(__dirname, `logs/timecard${post}.json`),
      data,
      'utf-8'
    )
  }
}
