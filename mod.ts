import Masto, {
  Notification,
  Status,
  CreateStatusParams,
  EventHandlerImpl,
} from 'masto'
import { Bbop } from './Bbop'
import { Butimili } from './Butimili'
import { Markov } from './Markov'
import { Smap } from './Smap'
import { Working } from './Working'
import { isMidnight } from './Fnc'

class Mstdn {
  private client?: Masto
  private inReplyToAcct?: string
  private inReplyToId?: string
  private visibility: CreateStatusParams['visibility'] = 'unlisted'
  stream?: EventHandlerImpl

  setReplyToken(
    accountAcct: string,
    id: string,
    visibility: CreateStatusParams['visibility'] = this.visibility
  ): void {
    this.inReplyToAcct = accountAcct
    this.inReplyToId = id
    this.visibility = visibility
  }
  resetReplyToken() {
    this.inReplyToAcct = undefined
    this.inReplyToId = undefined
    this.visibility = 'unlisted'
  }
  reply(text: string, visibility?: CreateStatusParams['visibility']): void {
    if (!this.inReplyToId || !this.inReplyToAcct) {
      console.error(
        `[ERR] ${new Date().toString()}: Reply failed. inReplyToId:"${
          this.inReplyToId
        }", inReplyToAcct:"${this.inReplyToAcct}" text:"${text}"`
      )
    }
    this.client
      ? this.client.createStatus({
          status: `@${this.inReplyToAcct} ${text}`,
          inReplyToId: this.inReplyToId,
          visibility: visibility || this.visibility,
        })
      : undefined
    this.resetReplyToken()
  }
  toot(
    text: string,
    visibility?: CreateStatusParams['visibility'],
    suffix = '#bot'
  ) {
    return this.client
      ? this.client.createStatus({
          status: [text, suffix].join(' ').trim(),
          visibility: visibility || this.visibility,
        })
      : Promise.reject()
  }

  follow(id: string) {
    return this.client ? this.client.followAccount(id) : Promise.reject()
  }

  async login() {
    this.client = await Masto.login({
      uri: 'https://social.mikutter.hachune.net',
      accessToken: process.env.TOKEN,
    }).catch(() => {
      throw new Error('connect error')
    })
    this.stream = await this.client.streamUser().catch()
  }

  hasEstablished() {
    return this.client !== undefined && this.stream !== undefined
  }
}

async function main(): Promise<void> {
  const mstdn = new Mstdn()
  await mstdn.login()
  const markov = new Markov()
  const butimili = new Butimili()
  const working = new Working()

  if (mstdn.stream === undefined) {
    throw new Error('Login failue.')
  }

  mstdn.stream.on('notification', async (notification: Notification) => {
    // console.log(notification)
    if (notification.type !== 'mention' || !notification.status) {
      return
    }
    const status: Status = notification.status
    const content = status.content
      .replace(/<.+?>/g, '')
      .replace(/@\S+(@\S+)?/g, '')
      .trim()
    const visibility =
      status.visibility === 'public' ? 'unlisted' : status.visibility
    mstdn.setReplyToken(status.account.acct, status.id, visibility)

    if (status.account.acct === 'sota_n') {
      if (/^halt/.test(content)) {
        mstdn
          .toot('【SYSTEM】 すやぽよー！すやぽよー！', 'unlisted', '')
          .finally(() => {
            process.exit()
          })
        return
      }

      if (/^say /.test(content)) {
        mstdn.toot(`【管理者便り】 ${content.substr(4)}`, status.visibility, '')
        return
      }

      if (/^(勤労|労働|勤務)(はじめ|開始)/.test(content)) {
        const ctx = content.replace(/(勤労|労働|勤務)(はじめ|開始)/, '')
        const [flg, returnNote] = working.attendance(ctx)
        if (flg) {
          mstdn.reply(`タイムカード押したぞ（${returnNote}）`)
        } else {
          mstdn.reply(`なんか失敗したぞ： ${returnNote}`)
        }
        return
      }
      if (/^(勤労|労働|勤務)(おわり|終了)/.test(content)) {
        const ctx = content.replace(/(勤労|労働|勤務)(おわり|終了)/, '')
        const [flg, returnNote] = working.leaving(ctx)
        if (flg) {
          mstdn.reply(`乙（${returnNote}）`)
        } else {
          mstdn.reply(`なんか失敗したぞ： ${returnNote}`)
        }
        return
      }
    }

    switch (content) {
      case 'bbop': {
        const bbop = new Bbop().do()
        mstdn.reply(`${bbop.Result} (${bbop.Match.Ratio.toFixed(2)}%)`)
        break
      }
      case 'ping': {
        mstdn.reply('pong')
        break
      }
      case 'smap': {
        const smap = new Smap().do()
        mstdn.reply(smap.Result)
        break
      }
      case 'btml': {
        mstdn.reply(butimili.btml(`@${status.account.acct}`))
        break
      }
      case 'help': {
        mstdn.reply('commands: bbop / ping / smap / btml')
        break
      }
      case 'フォロバして': {
        // console.log(status)
        const isLocalAcct = status.account.acct === status.account.username
        if (isLocalAcct) {
          // const Relationships = await client
          //   .fetchAccountRelationships([status.account.id])
          //   .catch()
          // const isFollowAlready = Relationships[0].following
          // console.log(Relationships, isFollowAlready)
          // if (!isFollowAlready) {
          mstdn
            .follow(status.account.id)
            .then(() => {
              mstdn.reply('したよ（できたとは言ってない）')
            })
            .catch((err) => {
              console.log(err)
            })
          // } else {
          // returnStr = 'もうしてるでしょ'
          // }
        } else {
          mstdn.reply(
            'リモートアカウントの自動フォロバは現在対応しておりません。忘れた頃に手動でやります。'
          )
          // client.followAccountByUsername(status.account.acct).catch()
        }
        break
      }
      default:
        mstdn.reply(markov.pickTweet())
        break
    }
  })

  setInterval(() => {
    if (!isMidnight()) {
      mstdn.toot(markov.pickTweet())
    }
  }, 20 /* min */ * 60 /* s */ * 1000 /* ms */)

  setInterval(() => {
    do {
      mstdn.login()
      console.log('relogin...')
    } while (!mstdn.hasEstablished())
  }, 24 * 60 * 60 * 1000)

  process.on('exit', async function () {
    await mstdn.toot('【SYSTEM】 すやぽよー！すやぽよー！')
    console.log('Exitting...')
  })

  process.on('SIGINT', function () {
    process.exit(0)
  })
  if (!process.env.TS_NODE_DEV) {
    mstdn.toot('【SYSTEM】ｽﾃﾝﾊﾞｰｲ')
  }
}

main()
