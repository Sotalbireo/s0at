import { isAprilFool } from './Fnc'

interface SmapResult {
  Result: string
}

export class Smap {
  private expect = '稲垣・木村・中居・香取・草彅'
  private material = ['垣', '村', '居', '取', '彅']
  private template = '稲%・木%・中%・香%・草%'
  private isAprilFool = isAprilFool()

  do(): SmapResult {
    if (this.isAprilFool) {
      return {
        Result: this.expect,
      }
    }
    const Result = this.template.replace(
      /%/g,
      () => this.material[Math.floor(Math.random() * 5)]
    )
    return {
      Result,
    }
  }
}
