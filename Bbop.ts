import { isAprilFool } from './Fnc'

interface BbopResult {
  Result: string
  Match: BbopCount
}

interface BbopCount {
  Count: number
  Ratio: number
}

export class Bbop {
  private ANSWER = 'ビ,ビ,ッ,ド,レ,ッ,ド,・,オ,ペ,レ,ー,ショ,ン'
  private MATERIAL = 'ビ,ビ,ド,レ,ド,オ,ペ,レ,ショ'
  private isAprilFool = isAprilFool()

  do = (): BbopResult => {
    if (this.isAprilFool) {
      return {
        Result: this.ANSWER.replace(/,/g, ''),
        Match: {
          Count: -1,
          Ratio: 100,
        },
      }
    }
    const answerArray = this.ANSWER.split(',')
    const materialArray = this.MATERIAL.split(',')

    const baseArray = this.ANSWER.split(',')

    answerArray.forEach((v, i) => {
      switch (v) {
        case 'ッ':
        // fall through
        case '・':
        // fall through
        case 'ー':
        // fall through
        case 'ン':
          return
        default: {
          const idx = Math.floor(Math.random() * 9)
          baseArray[i] = materialArray[idx]

          break
        }
      }
    })

    return {
      Result: baseArray.join(''),
      Match: this.CalcMatchCount(baseArray),
    }
  }

  private CalcMatchCount = (result: string[]): BbopCount => {
    const answerArray = this.ANSWER.split(',')
    let count = 0

    answerArray.forEach((ans, i) => {
      switch (ans) {
        case 'ッ':
        // fall through
        case '・':
        // fall through
        case 'ー':
        // fall through
        case 'ン':
          return
        default:
          if (result[i] === ans) count++
      }
    })

    return {
      Count: count,
      Ratio: (count / this.MATERIAL.length) * 100,
    }
  }
}
