import dayjs from 'dayjs'
import objectSupport from 'dayjs/plugin/objectSupport'
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter'
import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'

dayjs.extend(objectSupport)
dayjs.extend(isSameOrAfter)
dayjs.extend(utc)
dayjs.extend(timezone)
dayjs.tz.setDefault(process.env.TIMEZONE)

declare global {
  interface String {
    drop(idx: number, length: number): string
  }
  interface Array<T> {
    bracketOptimise(): Array<string>
  }
}

export function isAprilFool(v?: string | number | Date | dayjs.Dayjs): boolean {
  return dayjs(v).tz().format('M#D') === '4#1'
}

/**
 * is Between 23:00 to 7:00
 */
export function isMidnight(): boolean {
  const now = dayjs().tz()
  const today0700 = dayjs().tz().set({ hour: 7, minute: 0, second: 0 })
  const today2300 = dayjs().tz().set({ hour: 23, minute: 0, second: 0 })
  return now.isBefore(today0700, 'm') || now.isSameOrAfter(today2300, 'm')
}

export function isLatin(string: string): boolean {
  return /^[A-z\u00C0-\u00ff\s'.,-/#!$%^&*;:{}=\-_`~()]+$/.test(string)
}

function count(string: string, regexp: RegExp): number {
  return (string.match(regexp) || []).length
}

type bracketOptimiseStack = {
  idx: number
  txt: string
}

Array.prototype.bracketOptimise = function () {
  const self = this as string[]

  // 全くないなら即時返す
  if (
    !['(', ')', '（', '）', '「', '」', '『', '』', '｢', '｣'].some((bra) =>
      self.includes(bra)
    )
  ) {
    return self
  }
  // 数える
  const stack: bracketOptimiseStack[] = []
  self.forEach((str, idx) => {
    if (/[()（）「」『』｢｣]/.test(str)) {
      stack.push({ idx, txt: str })
    }
  })
  // 文中単一のカッコを消す
  if (stack.length === 1 && /[)）「『｢]/.test(stack[0].txt)) {
    self.splice(stack[0].idx, 1)
  }
  // こっちは読点にした方が通りが良さそうなのでそのように。
  if (stack.length === 1 && /[」』｣]/.test(stack[0].txt)) {
    self.splice(stack[0].idx, 1, '、')
  }
  console.log(stack)
  return self
}

String.prototype.drop = function (idx: number, length = 1) {
  return this.slice(0, idx) + this.slice(idx + length)
}
